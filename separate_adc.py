import os
import time
import csv
from tqdm import tqdm

log_dir = '/home/CanDumpAnalysis/SeparatedNodes/'
save_dir = '/home/CanDumpAnalysis/SeparatedADC/'

COBID_LIST = [0x000, 0x080, 0x100, 0x180, 0x200, 0x280, 0x300, 0x380, 0x400, 0x480,
              0x500, 0x580, 0x600, 0x700, 0x7E4, 0x7E5]

COMMUNICATION_OBJECT = ['NMT node control', 'Emergency', 'Timestamp', 'TPDO1', 'RPDO1',
                        'TPDO2', 'RPDO2', 'TPDO3', 'RPDO3', 'TPDO4', 'RPDO4', 'SDO transmit', 'SDO receive',
                        'NMT Node Monitoring', 'LSS transmit', 'LSS receive']

def separate_adc():
    log_folders = os.listdir(log_dir)
    for folders in log_folders:
        bus_dir = log_dir + str(folders) + '/'
        bus_folders = os.listdir(bus_dir)

        for busses in bus_folders:
            file_dir = bus_dir + str(busses) + '/'
            can_files = os.listdir(file_dir)
            for files in can_files:
                node_dir = file_dir + str(files)
                with open(node_dir) as in_file:
                    for line in tqdm(in_file, desc=str(node_dir)):
                        nodeid, cobid, data, timestamp = extract_data_from_line(line)
                        save_item_dir = save_dir + str(nodeid) + '/'

                        if not os.path.exists(save_item_dir):
                            os.makedirs(save_item_dir)

                        if int(cobid, 16) == 0x280:
                            decoded_data = decode_data(data, cobid)

                            if not decoded_data == ValueError:
                                save_name = save_dir + str(nodeid) + '/' + str(decoded_data[0]) + '.csv'
                                with open(save_name, 'a') as fd:
                                    writer = csv.writer(fd)
                                    csv_data = [timestamp, str(decoded_data[2]), str(decoded_data[1])]
                                    writer.writerow(csv_data)


def extract_data_from_line(line):
    formatted_line = str(line).replace(" ", "")  # Remove spaces to maintain structure
    identifier = str(formatted_line[31:34])  # Find identifier in string
    timestamp = str(formatted_line[1:26])               # Find timestamp in string
    timestamp = timestamp[0:10] + ' ' + timestamp[10:]  # Re add space from formatting line
    data = handle_data(formatted_line)
    cobid, nodeid = decode_cobid(identifier)
    return nodeid, cobid, data, timestamp


def cobid_to_comObj(cobid, data):
    try:
        index = COBID_LIST.index(int(cobid, 16))
        if index == 1 and len(data) == 0:
            return "Sync"
    except ValueError:
        return 'COBID was not recognised'
    return COMMUNICATION_OBJECT[index]


def decode_data(data, cobid):
    decoded_data = []

    if int(cobid, 16) == 0x180:  # TPDO1
        portf = bin(int(data[0], 16))
        porta = bin(int(data[1], 16))
        decoded_data = 'PortF in:', portf, 'PortA in: ', porta

    elif int(cobid, 16) == 0x200:  # RPDO1
        portc = bin(int(data[0], 16))
        porta = bin(int(data[1], 16))
        decoded_data = 'PortC out:', portc, 'PortA out: ', porta

    elif int(cobid, 16) == 0x280:     # TPDO2
        if not len(data) == 4:
            return ValueError

        channel = int(data[0], 16)
        chan_status = bin(int(data[1], 16))
        ADCvalue = int(data[3] + data[2], 16)
        decoded_data = [channel, chan_status, ADCvalue]

    elif int(cobid, 16) == 0x380:   # TPDO3
        channel = int(data[0], 16)
        chan_status = bin(int(data[1], 16))
        ADCvalue = int(data[2] + data[3] + data[4] + data[5], 16)
        decoded_data = 'Channel: ', str(channel), 'Channel Status: ', str(chan_status), 'ADC Value: ', str(ADCvalue)

    return decoded_data


def decode_cobid(identifier):
    try:
        bin_ids = int(identifier, 16)
        node_id = bin_ids & 127         # Node ID is the 7 lsb
        cob_id = hex(bin_ids & 1920)    # COBID is the 4 msb of 11 bit value
    except ValueError:
        return 'Invalid bins', 'Error'
    return cob_id, node_id


def handle_data(line):
    amnt_msg = int(line[line.find('[')+1:line.find(']')])
    msg = []
    if amnt_msg:
        for index in range(amnt_msg):
            msg.append(line[37 + (index * 2):39 + (index * 2)])
    else:
        msg = str(line[line.find(']') + 1:])
    return msg


if __name__ == '__main__':
    separate_adc()
