import os
import csv
import time
from tqdm import tqdm
from datetime import datetime

file_dir = '/home/CanDumpAnalysis/SeparatedADC/63/'
save_item_dir12 = '/home/CanDumpAnalysis/SeparatedADC/63week12'
save_item_dir3 = '/home/CanDumpAnalysis/SeparatedADC/63week3'
separate_msg = datetime.strptime('2018-08-15 10:42:13.927404', '%Y-%m-%d %H:%M:%S.%f')
week3_split = datetime.strptime('2018-08-18 18:35:49.102386', '%Y-%m-%d %H:%M:%S.%f')


if not os.path.exists(save_item_dir12):
    os.makedirs(save_item_dir12)
if not os.path.exists(save_item_dir3):
    os.makedirs(save_item_dir3)

folder = os.listdir(file_dir)

for files in folder:
    save_dir12 = save_item_dir12 + '/' + str(files)
    save_dir3 = save_item_dir3 + '/' + str(files)

    with open(file_dir + files, 'r') as csvfile:
        reader = csv.reader(csvfile)

        for row in tqdm(reader, desc=str(files)):
            timestamp = datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S.%f')

            if timestamp >= separate_msg:   # Week 3
                with open(save_dir3, 'a') as fd:
                    writer = csv.writer(fd)

                    if timestamp < week3_split:
                        csv_data = [row[0], str(int(row[1]) / 10), row[2]]
                    else:
                        csv_data = [row[0], row[1], row[2]]

                    writer.writerow(csv_data)
            else:                           # Week 1 and 2
                with open(save_dir12, 'a') as fd:
                    writer = csv.writer(fd)
                    csv_data = [row[0], row[1], row[2]]
                    writer.writerow(csv_data)
