import os
import time
import binascii
from stat import *
import csv
from datetime import datetime
from tqdm import tqdm

busses_dir = '/home/CanDumpAnalysis/SeparatedBusses/'
save_dir = '/home/CanDumpAnalysis/NodesFormatted/'

COBID_LIST = [0x000, 0x080, 0x100, 0x180, 0x200, 0x280, 0x300, 0x380, 0x400, 0x480,
              0x500, 0x580, 0x600, 0x700, 0x7E4, 0x7E5]

COMMUNICATION_OBJECT = ['NMT node control', 'Emergency', 'Timestamp', 'TPDO1', 'RPDO1',
                        'TPDO2', 'RPDO2', 'TPDO3', 'RPDO3', 'TPDO4', 'RPDO4', 'SDO transmit', 'SDO receive',
                        'NMT Node Monitoring', 'LSS transmit', 'LSS receive']


def separate_nodes():
    folders = os.listdir(busses_dir)
    for items in folders:

        bus_files = os.listdir(busses_dir + str(items) + '/')
        for files in bus_files:

            file_dir = busses_dir + str(items) + '/' + str(files)
            with open(file_dir) as in_file:

                for line in tqdm(in_file, ascii=True, desc=str(items) + ': ' + str(files)):

                    formatted_line = str(line).replace(" ", "")         # Remove spaces to maintain structure
                    identifier = str(formatted_line[31:34])             # Find identifier in string
                    timestamp = str(formatted_line[1:26])               # Find timestamp in string
                    timestamp = timestamp[0:10] + ' ' + timestamp[10:]  # Re add space from formatting line
                    timestamp = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')

                    data = handle_data(formatted_line)
                    cobid, nodeid = decode_cobid(identifier)
                    com_obj = cobid_to_comObj(cobid, data)

                    csv_data = [line, cobid, com_obj, data, timestamp]
                    write_csv(nodeid, csv_data)


def write_csv(nodeid, csv_data):
    if nodeid == 0: # Master counts all nodes
        for index in range(128):
            file_dir = save_dir + str(index) + '.csv'
            with open(file_dir, 'a') as fd:
                writer = csv.writer(fd)
                writer.writerow(csv_data)
    else:
        file_dir = save_dir + str(nodeid) + '.csv'
        with open(file_dir, 'a') as fd:
            writer = csv.writer(fd)
            writer.writerow(csv_data)


def cobid_to_comObj(cobid, data):
    try:
        index = COBID_LIST.index(int(cobid, 16))
        if index == 1 and len(data) == 0:
            return "Sync"
    except ValueError:
        return 'COBID was not recognised'
    return COMMUNICATION_OBJECT[index]


def handle_data(line):
    amnt_msg = int(line[line.find('[')+1:line.find(']')])
    msg = []
    if amnt_msg:
        for index in range(amnt_msg):
            msg.append(line[37 + (index * 2):39 + (index * 2)])
    else:
        msg = str(line[line.find(']') + 1:])
    return msg


def decode_data(data, cobid):
    decoded_data = []

    if int(cobid, 16) == 0x180:  # TPDO1
        portf = bin(int(data[0], 16))
        porta = bin(int(data[1], 16))
        decoded_data = 'PortF in:', portf, 'PortA in: ', porta

    elif int(cobid, 16) == 0x200:  # RPDO1
        portc = bin(int(data[0], 16))
        porta = bin(int(data[1], 16))
        decoded_data = 'PortC out:', portc, 'PortA out: ', porta

    elif int(cobid, 16) == 0x280:     # TPDO2
        channel = int(data[0], 16)
        chan_status = bin(int(data[1], 16))
        ADCvalue = int(data[2] + data[3], 16)
        decoded_data = 'Channel: ', str(channel), 'Channel Status: ', str(chan_status), 'ADC Value: ', str(ADCvalue)

    elif int(cobid, 16) == 0x380:   # TPDO3
        channel = int(data[0], 16)
        chan_status = bin(int(data[1], 16))
        ADCvalue = int(data[2] + data[3] + data[4] + data[5], 16)
        decoded_data = 'Channel: ', str(channel), 'Channel Status: ', str(chan_status), 'ADC Value: ', str(ADCvalue)

    return decoded_data


def decode_cobid(identifier):
    try:
        bin_ids = int(identifier, 16)
        node_id = bin_ids & 127         # Node ID is the 7 lsb
        cob_id = hex(bin_ids & 1920)    # COBID is the 4 msb of 11 bit value
    except ValueError:
        return 'Invalid bins', 'Error'
    return cob_id, node_id


if __name__ == '__main__':
    separate_nodes()
