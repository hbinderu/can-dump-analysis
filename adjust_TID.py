import csv
import os
import time
from datetime import datetime


def timestamp_parser(timestamp):
    return datetime.strptime(timestamp, '%d/%m/%Y %H:%M:%S')


tid_dir = '/afs/cern.ch/user/h/hbinderu/Documents/radmon_elmb2.csv'
max_dose1 = 588.4966032
max_dose2 = 1013.597506
heh_dose1 = 2259520000000
heh_dose2 = 3891690000000

timestamp1 = timestamp_parser('08/08/2018 15:05:25')
timestamp2 = timestamp_parser('15/08/2018 17:15:05')


with open(tid_dir) as csvfile:
    reader = csv.reader(csvfile)

    with open('TID_full.csv', 'a') as fd:
        writer = csv.writer(fd)

        for row in reader:
            try:
                timestamp = timestamp_parser(str(row[0]))
                if timestamp1 <= timestamp < timestamp2:
                    data_tid = (float(row[5]) - max_dose1) / 10
                    data_heh = float(row[4]) - heh_dose1
                    writer.writerow([row[0], str(data_tid), str(data_heh)])
                elif timestamp >= timestamp2:
                    data_tid = (float(row[5]) - max_dose2) / 10
                    data_heh = float(row[4]) - heh_dose2
                    writer.writerow([row[0], str(data_tid), str(data_heh)])
                else:
                    writer.writerow([row[0], str(float(row[5]) / 10), str(row[4])])
            except ValueError:
                print ValueError
