import os
import csv
import xlrd
from collections import OrderedDict
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from EmergencyMsg import Translator
from time import sleep
from tqdm import tqdm

LINE_INDEX = 0
COB_ID_INDEX = 1
COM_OBJ_INDEX = 2
DATA_INDEX = 3
TIMESTAMP_INDEX = 4


class Nodes:
    def __init__(self, nodeid):
        self._nodeid = nodeid
        self.__previous_nodeguard = None
        self.__previous_digital = None
        self.__previous_cobid = None

        self.fix_msg = None

        self.__previous_ADC_msg = None
        self.ADC_reason = None
        self.new_ADC_error = True
        self.last_sync_timestamp = None
        self.last_boot_up_msg = None
        self.last_soft_reset = None

        self.new_error = True
        self.digital_response = True
        self.new_digital_error = True
        self.amount_digital_errors = 0

        self.__analog_status = []
        self.digital_values = [[], []]
        self. nodeguard_values = [[], []]
        self.ADC_values = [[], []]
        self.power_dict = None

        self.ADC_responsive = True
        self.nodeguard_responsive = True
        self.digital_responsive = True

        self.ADC_statistics = [[], []]
        self.nodeguard_statistics = [[], []]
        self.digital_statistics = [[], []]
        self.board_statistics = [[], []]

    def set_previous_ADC_msg(self, value):
        self.__previous_ADC_msg = value

    def get_previous_ADC_msg(self):
        return self.__previous_ADC_msg

    def get_previous_cobid(self):
        return self.__previous_cobid

    def set_previous_cobid(self, value):
        self.__previous_cobid = value

    def get_previous_nodeguard(self):
        return self.__previous_nodeguard

    def set_previous_nodeguard(self, value):
        self.__previous_nodeguard = value

    def get_previous_digital(self):
        return self.__previous_digital

    def set_previous_digital(self, data):
        self.__previous_digital = data

    def get_nodeid(self):
        return self._nodeid

    def set_analog_status(self, data):
        self.__analog_status = data

    def get_analog_status(self):
        return self.__analog_status


class CandumpAnalysis:
    file_dir = '/home/CanDumpAnalysis/NodesFormatted/'
    digital_on = 1
    digital_off = 0
    nodeguard_on = 3
    nodeguard_off = 2
    ADC_on = 5
    ADC_off = 4

    nodes_offline_start = [datetime.strptime('2018-08-03 16:59:00', '%Y-%m-%d %H:%M:%S')]
    nodes_offline_stop = [datetime.strptime('2018-08-03 18:48:00', '%Y-%m-%d %H:%M:%S')]

    def __init__(self):
        self.nodeObj = []
        self.nodeReg = []

    def analyse_file(self, nodeid):
        self.create_node_objs()
        nodeObj = self.nodeObj[nodeid]

        analog = self.extract_power_events(nodeid, 'analog')
        digital = self.extract_power_events(nodeid, 'digital')
        can = self.extract_power_events(nodeid, 'can')

        if not analog:  # No power information for the board - invalid data
            return

        # Adding analog and digital power to nodeObj for checking comparing power events
        power_dict = OrderedDict([('analog', analog), ('digital', digital), ('can', can)])
        nodeObj.power_dict = power_dict
        nodeObj.set_analog_status(analog)

        EmgTranslator = Translator()

        file_name = self.file_dir + str(nodeid) + '.csv'
        save_dir = 'Error Messages/' + 'nodeid_' + str(nodeid) + '.txt'
        statistics_dir = 'Statistics/' + 'nodeid_' + str(nodeid) + '.txt'

        with open(file_name, 'r') as fd:
            reader = csv.reader(fd)

            boot_loader_time = datetime.today()
            wait_for_data = True

            timestamp = None
            last_timestamp = None

            for i, line in enumerate(reader):
                cobid = line[COB_ID_INDEX]
                data = line[DATA_INDEX]

                # As files are merged with master files, we want to know when the actual data starts
                if wait_for_data:
                    if cobid == '0x80' or cobid == '0x600' or cobid == '0x0' or cobid == '0x280':
                        # print line
                        continue
                    else:
                        try:  # To avoid format errors from the candump
                            timestamp = datetime.strptime(line[TIMESTAMP_INDEX], '%Y-%m-%d %H:%M:%S.%f')
                        except ValueError:
                            timestamp = datetime.strptime(line[TIMESTAMP_INDEX], '%Y-%m-%d %H:%M:%S')
                        last_timestamp = timestamp
                        nodeObj.digital_values[0].append(timestamp)
                        nodeObj.digital_values[1].append(self.digital_on)
                        nodeObj.nodeguard_values[0].append(timestamp)
                        nodeObj.nodeguard_values[1].append(self.nodeguard_on)
                        nodeObj.ADC_values[0].append(timestamp)
                        nodeObj.ADC_values[1].append(self.ADC_on)
                        wait_for_data = False

                try:    # To avoid format errors from the candump
                    timestamp = datetime.strptime(line[TIMESTAMP_INDEX], '%Y-%m-%d %H:%M:%S.%f')
                except ValueError:
                    timestamp = datetime.strptime(line[TIMESTAMP_INDEX], '%Y-%m-%d %H:%M:%S')

                time_diff = timestamp - last_timestamp
                if not time_diff > timedelta(seconds=0):
                    continue

                last_timestamp = timestamp

                if self.check_if_nodes_offline(timestamp):
                    continue

                state_analog = self.compare_power_state(timestamp, analog)     # Check if board is supplied
                state_digital = self.compare_power_state(timestamp, digital)     # Check if board is supplied
                state_can = self.compare_power_state(timestamp, can)     # Check if board is supplied

                if cobid == '0x80':
                    try:
                        emg_msg = EmgTranslator(line)
                        if emg_msg[0] == 'Bootloader is now in control' or emg_msg[0] == 'Irregular reset (Watchdog, Brown-out or JTAG)' or emg_msg[0] == 'Life Guarding':
                            boot_loader_time = timestamp + timedelta(seconds=10)

                            # Resetting the previous nodeguard
                            nodeObj.set_previous_nodeguard(None)

                            # Adding the last boot-up msg to the object, so we can extract it
                            nodeObj.last_boot_up_msg = emg_msg[0], timestamp

                        if self.compare_boot_time(timestamp, nodeObj):
                            # If we're within a minute a minute after bootup - this is the reason for fix
                            nodeObj.fix_msg = nodeObj.last_boot_up_msg[0]
                        else:
                            nodeObj.fix_msg = None

                        if (emg_msg[0] == 'Bootloader is now in control' or 'ADC' in emg_msg[0] or emg_msg[0] == 'SYNC') and timestamp >= boot_loader_time and state_analog and state_digital and state_can:
                            self.adc_analyse(nodeObj, cobid, emg_msg[0], line, timestamp, i, save_dir)

                    except ValueError as msg:
                        print msg
                        sleep(0.5)

                if timestamp >= boot_loader_time:
                    if state_digital and state_can and cobid == '0x700' and 'remoterequest' not in data:
                        self.nodeguard_analyse(nodeObj, data, timestamp, i, save_dir)
                    if state_digital and state_can and cobid == '0x580' or cobid == '0x600':
                        self.digital_trans_analyse(nodeObj, cobid, line, timestamp, i, save_dir)
                    if state_analog and state_digital and state_can and cobid == '0x280':
                        self.adc_analyse(nodeObj, cobid, None, line, timestamp, i, save_dir)

                    if cobid == '0x700' and '00' in data:
                        nodeObj.last_soft_reset = timestamp

            self.save_statistics(statistics_dir, nodeObj)
            # self.plot_timeline(nodeObj, timestamp)

    def check_if_board_is_responsive(self, nodeObj):
        if nodeObj.ADC_responsive and nodeObj.digital_responsive and nodeObj.nodeguard_responsive:
            return True
        else:
            return False

    def check_if_nodes_offline(self, timestamp):
        for index in range(len(self.nodes_offline_start)):
            if self.nodes_offline_start[index] <= timestamp <= self.nodes_offline_stop[index]:
                return True
        return False

    def plot_timeline(self, nodeObj, timestamp):
        # Add last initial value
        last_nodeguard_value = nodeObj.nodeguard_values[1][-1]
        last_digital_value = nodeObj.digital_values[1][-1]
        last_ADC_value = nodeObj.ADC_values[1][-1]

        nodeObj.nodeguard_values[0].append(timestamp)
        nodeObj.nodeguard_values[1].append(last_nodeguard_value)
        nodeObj.digital_values[0].append(timestamp)
        nodeObj.digital_values[1].append(last_digital_value)
        nodeObj.ADC_values[0].append(timestamp)
        nodeObj.ADC_values[1].append(last_ADC_value)

        plt.plot(nodeObj.nodeguard_values[0], nodeObj.nodeguard_values[1], 'r', label='Nodeguard')
        plt.plot(nodeObj.digital_values[0], nodeObj.digital_values[1], 'g', label='Digital IO')
        plt.plot(nodeObj.ADC_values[0], nodeObj.ADC_values[1], 'B', label='ADC')
        plt.xlabel('Timestamp')
        plt.legend()
        plt.show()

    def adc_analyse(self, nodeObj, cobid, msg, line, timestamp, lineindex, save_dir):
        pre_msg = nodeObj.get_previous_ADC_msg()

        error_msg = None

        if cobid == '0x80':
            if msg == 'SYNC' and not pre_msg == 'SYNC':
                # Correct response
                nodeObj.last_sync_timestamp = timestamp
                pass
            elif pre_msg == 'SYNC' and msg == 'SYNC':
                if self.compare_sync(timestamp, nodeObj):
                    # Two syncs in a row
                    error_msg = 'No ADC response from SYNC'
            elif 'SYNC' not in msg:
                # If it's not sync it's emergency from ADC - hence the reason for error
                nodeObj.ADC_reason = msg
                return

            nodeObj.set_previous_ADC_msg(msg)

        if cobid == '0x280':
            nodeObj.set_previous_ADC_msg(cobid)

        if error_msg:
            # Print the reason for having the error
            if nodeObj.new_ADC_error:

                nodeObj.ADC_responsive = False

                # %%%%%%%%%%%%%%%%% ADC ERRORS START HERE %%%%%%%%%%%%%%%%%%%%%%%%
                self.add_data_to_timeline(timestamp, self.ADC_off, nodeObj)
                self.print_and_write_txt(save_dir, '\t\tADC Error: \tLine: ' + str(lineindex) + '\t' + str(timestamp) + ' Msg: ' + str(error_msg) + ' - START\n')

                nodeObj.new_ADC_error = False
        else:
            # Check if it's a new error cycle
            if not nodeObj.new_ADC_error:

                # Get the reason for error
                if nodeObj.ADC_reason:
                    reason = 'Reason: ' + str(nodeObj.ADC_reason)
                else:
                    reason = 'Reason: Unknown'

                # Get the reason for fixing the error
                solution = self.find_solution(timestamp, nodeObj)

                # self explanatory
                self.print_and_write_txt(save_dir, '\t\tADC Error: \tLine: ' + str(lineindex) + '\t' + str(timestamp) + ' - STOP\n')
                self.print_and_write_txt(save_dir, '\t\t' + str(reason) + ' ' + str(solution) + '\n')
                self.add_to_statistics(solution, 'adc', nodeObj)

                nodeObj.ADC_responsive = True
                if self.check_if_board_is_responsive(nodeObj):
                    self.add_to_statistics(solution, 'board', nodeObj)

                nodeObj.ADC_reason = None
                # %%%%%%%%%%%%%%%%%%%%% ADC ERRORS STOP HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                self.add_data_to_timeline(timestamp, self.ADC_on, nodeObj)

            nodeObj.new_ADC_error = True

    def find_solution(self, timestamp, nodeObj):
        # Get the reason for fixing the error
        power_reset = self.compare_power_timestamp(timestamp, nodeObj)
        soft_reset = self.compare_soft_reset(timestamp, nodeObj)

        if power_reset:
            solution = 'Solution: External Reset - ' + str(power_reset)
        elif soft_reset:
            solution = 'Solution: Soft Reset'
        else:
            if nodeObj.fix_msg:
                solution = 'Solution: ' + nodeObj.fix_msg
            else:
                solution = 'Solution: Self-recovery'
        return solution

    def compare_soft_reset(self, timestamp, nodeObj):
        try:
            time_diff = timestamp - nodeObj.last_soft_reset
        except TypeError:
            return False

        if time_diff > timedelta(seconds=60):
            return False
        else:
            return True

    def compare_power_timestamp(self, timestamp, nodeObj):
        rv = []
        for key, items in nodeObj.power_dict.iteritems():
            for values in items:
                time_diff = timestamp - values[1]
                if timedelta(seconds=0) <= time_diff <= timedelta(minutes=5):
                    rv.append(key)

        return list(set(rv))

    def compare_boot_time(self, timestamp, nodeObj):
        try:
            time_diff = timestamp - nodeObj.last_boot_up_msg[1]
        except TypeError:
            return False

        if time_diff > timedelta(seconds=60):
            return False
        else:
            return True

    def compare_sync(self, timestamp, nodeObj):
        try:
            time_diff = timestamp - nodeObj.last_sync_timestamp
        except TypeError:
            return True

        if time_diff > timedelta(seconds=25):
            return True
        else:
            return False

    def digital_trans_analyse(self, nodeObj, cobid, line, timestamp, lineindex, save_dir):
        data = self.extract_data(line)

        error_msg = None

        if not (data[1] == '00' and data[2] == '62' and data[3] == '01'):
            return

        if cobid == '0x600':
            if nodeObj.get_previous_cobid() == '0x600' or not nodeObj.digital_response:
                error_msg = 'No response..'

            if not data[4] == '00':
                nodeObj.set_previous_digital(data[4])

            nodeObj.digital_response = False

        elif cobid == '0x580':
            if nodeObj.get_previous_cobid() == '0x580':
                error_msg = 'Multiple response..'
            else:
                if not data[4] == '00':
                    if not (data[4] == nodeObj.get_previous_digital() or nodeObj.get_previous_digital() is None):
                        error_msg = 'incorrect response'
                else:
                    if not data[4] == '00':
                        error_msg = 'incorrect response'

        if error_msg:
            nodeObj.amount_digital_errors += 1
            if cobid == '0x580':
                nodeObj.digital_response = False
            if nodeObj.new_digital_error:
                self.print_and_write_txt(save_dir, '\tDigital Error: \tLine: ' + str(lineindex) + '\t' + str(timestamp) + ' Msg: ' + str(error_msg) + ' - START\n')
                nodeObj.new_digital_error = False
                self.add_data_to_timeline(timestamp, self.digital_off, nodeObj)
                nodeObj.digital_responsive = False
        else:
            nodeObj.digital_response = True
            if not nodeObj.new_digital_error:
                nodeObj.new_digital_error = True

                solution = self.find_solution(timestamp, nodeObj)

                self.print_and_write_txt(save_dir, '\tDigital Error: \tLine: ' + str(lineindex) + '\t' + str(timestamp) + ' Amount: ' + str(nodeObj.amount_digital_errors) + ' - STOP\n')
                self.print_and_write_txt(save_dir, '\t' + str(solution) + '\n')
                nodeObj.amount_digital_errors = 0
                self.add_data_to_timeline(timestamp, self.digital_on, nodeObj)
                self.add_to_statistics(solution, 'digital', nodeObj)

                nodeObj.digital_responsive = True
                if self.check_if_board_is_responsive(nodeObj):
                    self.add_to_statistics(solution, 'board', nodeObj)

        nodeObj.set_previous_cobid(cobid)

    def nodeguard_analyse(self, nodeObj, data, timestamp, line_index, save_dir):
        if nodeObj.get_previous_nodeguard() == data:
            if nodeObj.new_error:
                nodeObj.nodeguard_responsive = False
                nodeObj.new_error = False
                self.print_and_write_txt(save_dir, 'Nodeguard Error: \tLine: ' + str(line_index) + '\t' + str(timestamp) + ' - START\n')
                self.add_data_to_timeline(timestamp, self.nodeguard_off, nodeObj)
        else:
            if not nodeObj.new_error:
                nodeObj.nodeguard_responsive = True

                solution = self.find_solution(timestamp, nodeObj)
                self.add_to_statistics(solution, 'nodeguard', nodeObj)

                self.print_and_write_txt(save_dir, 'Nodeguard Error: \tLine: ' + str(line_index) + '\t' + str(timestamp) + ' - STOP\n')
                self.print_and_write_txt(save_dir, str(solution) + '\n')
                self.add_data_to_timeline(timestamp, self.nodeguard_on, nodeObj)

                if self.check_if_board_is_responsive(nodeObj):
                    self.add_to_statistics(solution, 'board', nodeObj)

                nodeObj.new_error = True
        nodeObj.set_previous_nodeguard(data)

    def add_data_to_timeline(self, timestamp, value, nodeObj):
        pre_timestamp = timestamp - timedelta(microseconds=1)
        if value == self.nodeguard_off or value == self.nodeguard_on:
            if value == self.nodeguard_off:
                pre_value = self.nodeguard_on
            else:
                pre_value = self.nodeguard_off
            nodeObj.nodeguard_values[0].append(pre_timestamp)
            nodeObj.nodeguard_values[1].append(pre_value)

            nodeObj.nodeguard_values[0].append(timestamp)
            nodeObj.nodeguard_values[1].append(value)
        elif value == self.digital_off or value == self.digital_on:
            if value == self.digital_off:
                pre_value = self.digital_on
            else:
                pre_value = self.digital_off
            nodeObj.digital_values[0].append(pre_timestamp)
            nodeObj.digital_values[1].append(pre_value)

            nodeObj.digital_values[0].append(timestamp)
            nodeObj.digital_values[1].append(value)
        elif value == self.ADC_off or value == self.ADC_on:
            if value == self.ADC_off:
                pre_value = self.ADC_on
            else:
                pre_value = self.ADC_off
            nodeObj.ADC_values[0].append(pre_timestamp)
            nodeObj.ADC_values[1].append(pre_value)

            nodeObj.ADC_values[0].append(timestamp)
            nodeObj.ADC_values[1].append(value)

    def print_and_write_txt(self, save_dir, text):
        with open(save_dir, 'a') as text_file:
            text_file.write(text)
        # print text[:-1]

    def compare_power_state(self, timestamp, power_status):
        index_range = reversed(range(len(power_status)))
        for index in index_range:
            state_time = power_status[index][1]
            if timestamp > state_time:
                state = power_status[index][0]   # Returns the state at the compared timestamp
                if state == 'ON':
                    return True
                else:
                    return False
        return True    # In case we have no power status from the time, we assume it's on

    def create_node_objs(self):
        can_files = os.listdir(self.file_dir)
        nodeids = []

        for files in can_files:
            nodeids.append(int(files[:-4]))
        nodeids.sort()

        for ids in nodeids:
            self.nodeObj.append(Nodes(ids))

    def extract_power_events(self, nodeid, type):
        if type == 'analog':
            x = 0
            y = 1
            z = 2
        elif type == 'digital':
            x = 4
            y = 5
            z = 6
        elif type == 'can':
            x = 8
            y = 9
            z = 10
        else:
            raise ValueError('Invalid power event type.\n Allowed: analog, digital, can')

        book = xlrd.open_workbook('PowerEvents.xlsx')
        sheet_names = book.sheet_names()
        try:
            sheet_index = sheet_names.index(str(nodeid))
        except ValueError:
            return False

        sh = book.sheet_by_index(sheet_index)
        data = []
        for rx in range(sh.nrows):
            analog_id = sh.row(rx)[x].value
            if 'Status' in analog_id:
                status = sh.row(rx)[y].value
                if 'ON' in status:
                    state = 'ON'
                else:
                    state = 'OFF'
                timestamp = datetime.strptime(sh.row(rx)[z].value, '%Y-%m-%d %H:%M:%S.%f')
                data.append([state, timestamp])

        return data

    def extract_data(self, line):
        length = int(line[0][line[0].find('[') + 1:line[0].find(']')])
        data = []
        for index in range(length):
            line_num = 2 + (index * 6)
            data.append(line[3][line_num:line_num+2])
        return data

    def add_to_statistics(self, data, type, nodeObj):
        if type == 'digital':
            stats = nodeObj.digital_statistics
        elif type == 'nodeguard':
            stats = nodeObj.nodeguard_statistics
        elif type == 'adc':
            stats = nodeObj.ADC_statistics
        elif type == 'board':
            stats = nodeObj.board_statistics
        else:
            raise ValueError('Please input digital, nodeguard, adc or board')

        error_msg = data[data.find(':') + 2:]
        try:
            index = stats[0].index(error_msg)
            stats[1][index] += 1
        except ValueError:
            stats[0].append(error_msg)
            stats[1].append(1)

    def save_statistics(self, dir, nodeObj):
        print nodeObj.digital_statistics
        print nodeObj.nodeguard_statistics
        print nodeObj.ADC_statistics

        with open(dir, 'a') as text_file:

            text_file.write('Digital IO:\n')

            for index in range(len(nodeObj.digital_statistics[0])):
                text = str(nodeObj.digital_statistics[0][index]) + ': ' + str(nodeObj.digital_statistics[1][index]) + '\n'
                text_file.write(text)

            text_file.write('\n\nNodeguards:\n')

            for index in range(len(nodeObj.nodeguard_statistics[0])):
                text = str(nodeObj.nodeguard_statistics[0][index]) + ': ' + str(nodeObj.nodeguard_statistics[1][index]) + '\n'
                text_file.write(text)

            text_file.write('\n\nADC:\n')

            for index in range(len(nodeObj.ADC_statistics[0])):
                text = str(nodeObj.ADC_statistics[0][index]) + ': ' + str(nodeObj.ADC_statistics[1][index]) + '\n'
                text_file.write(text)

            text_file.write('\n\n--------------------------------------------------\n\nBoard:\n')

            for index in range(len(nodeObj.board_statistics[0])):
                text = str(nodeObj.board_statistics[0][index]) + ': ' + str(nodeObj.board_statistics[1][index]) + '\n'
                text_file.write(text)


if __name__ == '__main__':
    CandumpAnalysis().analyse_file(51)
    # for index in tqdm(range(127)):
    #     CandumpAnalysis().analyse_file(index)

