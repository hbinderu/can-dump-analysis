
class Translator:
    def __init__(self):
        pass

    def __call__(self, line):
        data = self.extract_data(line)
        return self.handle_data(data)

    def handle_data(self, data):
        if not data:
            return ['SYNC', 'Broadcast']

        if not len(data) > 2:
            raise ValueError('Invalid error code')

        error_code = data[1] + data[0]
        if error_code == '8100':
            return self.handle_8100(data)
        elif error_code == '8130':
            return self.handle_8130(data)
        elif error_code == '8210':
            return self.handle_8210(data)
        elif error_code == '5000':
            return self.handle_5000(data)
        elif error_code == '6000':
            return self.handle_6000(data)
        else:
            raise ValueError('UNKNOWN ERROR CODE', error_code)

    def handle_8100(self, data):
        msg = ['CAN communication']

        byte_msg = {'3': ['Warning Level', 'Bus Off', 'Error Passive', 'Transmit Check', '81C91 INT register bits:'],
                    '4': ['Init Mode', 'Reset State', 'Bus Off', 'Receive Error Counter >= 96', 'Transmit Error Counter >= 96', 'last Transmission Complete', 'Receive Mode', 'Auto Decrement Address', '81C91 MODE/STATUS register bits:']}

        byte_register = {'3': ['04', '20', '40', '80'],
                         '4': ['01', '02', '04', '08', '10', '20', '40', '80']}

        for index in [3, 4]:
            try:
                error_byte = data[index]
            except (KeyError, IndexError):
                raise ValueError('Invalid error format')
            try:
                msg_index = byte_register[str(index)].index(error_byte)
                msg.append(byte_msg[str(index)][msg_index])
            except ValueError:
                size = len(byte_msg[str(index)])
                msg.append(str(byte_msg[str(index)][size-1]) + ' ' + str(error_byte))

        msg.append('Error counter: ' + str(data[5]))
        msg.append('Bus-off counter: ' + str(data[6]))

        return msg

    def handle_8130(self, data):
        msg = ['Life Guarding', 'CAN-controller has been reinitialized']
        return msg

    def handle_8210(self, data):
        msg = ['RPDO: too few bytes', 'Minimum DLC (Data Length Code) required: ' + str(data[3])]
        return msg

    def handle_5000(self, data):
        byte_msg = {'02': ['Reset-Valid bit not set', 'Reset-Valid bit not reset', 'error in Offset Register value', 'error in Gain Register value'],
                    '41': ['PDO communication parameters', 'Guarding parameters', 'ADC configuration', 'Digital I/O configuration', 'DAC configuration', 'CAN configuration parameters', 'Calibration constant(s)', 'ELMB Serial Number'],
                    '43': ['ADC delta-change values', 'ADC upper limits', 'ADC lower limits'],
                    'F0': ['Power-On Reset', 'External Reset', 'Brown-Out Reset', 'Watchdog Reset', 'JTAG Reset', 'JTAG Interface Disable']}

        byte_reg = {'02': ['01', '02', '04', '08'],
                    '41': ['00', '01', '02', '03', '04', '05', 'FE', 'FF'],
                    '43': ['01', '02', '03'],
                    'F0': ['01', '02', '04', '08', '10', '80']}

        error_byte = data[3]

        if error_byte == '01':
            if data[5] == '00':
                ADC_channel = int(data[4], 16)
                return ['ADC: Conversion timeout', 'ADC channel: ' + str(ADC_channel)]

        elif error_byte == '02':
            if data[4] == '00':
                try:
                    error_index = byte_reg['02'].index(data[5])
                    error_msg = byte_msg['02'][error_index]
                except ValueError:
                    error_msg = 'Invalid error msg'
                return ['ADC: Reset failed', 'Error ID: ' + str(error_msg)]

        elif error_byte == '03' and data[4] == '00':
            return 'ADC: Offset calibration failed'

        elif error_byte == '04' and data[4] == '00':
            return 'ADC: Gain calibration failed'

        elif error_byte == '10':
            return ['ADC problem(s) during initialisation', 'ADC status: ' + str(data[4])]

        elif error_byte == '11':
            return 'ADC calibration constants: not available'

        elif error_byte == '20':
            return 'Slave processor not responding (ELMB103only)'

        elif error_byte == '30':
            if data[4] == '01':
                status = 'Program Flash'
            elif data[4] == '02':
                status = 'Slave FLASH; ELMB103 only'
            else:
                status = 'Unknown register...'
            return ['CRC error', status]

        elif error_byte == '41':
            try:
                error_index = byte_reg['41'].index(data[4])
                error_msg = byte_msg['41'][error_index]
            except ValueError:
                error_msg = 'Invalid error msg'

            if data[5] == '00':
                size = 'While writing datablock info'
            else:
                size = 'Size of parameter block to write: ' + str(data[5])
            return ['EEPROM: write error', 'Parameter block index: ' + str(error_msg), size]

        elif error_byte == '42':
            try:
                error_index = byte_reg['41'].index(data[4])
                error_msg = byte_msg['41'][error_index]
            except ValueError:
                error_msg = 'Invalid error msg'

            if data[5] == '01':
                ids = 'ID: CRC'
            elif data[5] == '02':
                ids = 'Length: ' + str(data[5])
            elif data[5] == '04':
                ids = 'Infoblock'
            else:
                ids = 'Unknown error ID'
            return ['EEPROM: read error', 'Parameter block index: ' + str(error_msg), ids]

        elif error_byte == '43':
            try:
                error_index = byte_reg['43'].index(data[4])
                error_msg = byte_msg['43'][error_index]
            except ValueError:
                error_msg = 'Invalid error msg'
            return ['EEPROM: ADC-limits write error', 'Parameter block ID: ' + str(error_msg), 'Size of block to write: ' + str(data[5])]

        elif error_byte == 'F0':
            try:
                error_index = byte_reg['F0'].index(data[4])
                error_msg = byte_msg['F0'][error_index]
            except ValueError:
                error_msg = 'Invalid error msg'
            return ['Irregular reset (Watchdog, Brown-out or JTAG)', 'Microcontroller MCUCSR register contents: ' + str(error_msg)]

        elif error_byte == 'F1':
            return 'Bootloader: not present'

        elif error_byte == 'FE':
            if data[4] == '01' and data[5] == '28' and data[7] == '00':
                try:
                    error_index = byte_reg['F0'].index(data[6])
                    error_msg = byte_msg['F0'][error_index]
                except ValueError:
                    error_msg = 'Invalid error msg'
                return ['Bootloader is now in control', 'Microcontroller MCUCSR register contents: ' + str(error_msg)]
            else:
                return 'Emergency code 5000h - UNKNOWN ERROR: ' + str(error_byte)

        else:
            return 'Emergency code 5000h - UNKNOWN ERROR: ' + str(error_byte)

    def handle_6000(self, data):
        if data[3] == 'FE' and data[4] == 'AA' and data[5] == 'AA' and data[6] == '00' and data[7] == '00':
            return 'Bootloader cannot jump to application: invalid (Message generated by bootloader program)'
        else:
            return 'Error Code 6000 with invalid error-fields..'

    def extract_data(self, line):
        length = int(line[0][line[0].find('[') + 1:line[0].find(']')])
        data = []
        for index in range(length):
            line_num = 2 + (index * 6)
            data.append(line[3][line_num:line_num+2])
        return data

from time import sleep

def convert_hehe(line):
    print line

    next_index = line.find(':')
    name = line[:next_index]
    line = line[next_index + 1:]

    error_reg = []
    error_msg = []
    while True:
        start_index = line.find(':')
        end_index = line.find(',')
        error_reg.append(line[1 :start_index - 1])
        if end_index > 0:
            error_msg.append(line[start_index + 2:end_index])
        else:
            error_msg.append(line[start_index + 2:])
            break

        line = line[end_index + 1:]
    print error_reg
    print error_msg


# convert_hehe('ATmega128 MCUCSR register bits: 01h: Power-On Reset, 02h: External Reset, 04h: Brown-Out Reset, 08h: Watchdog Reset, 10h: JTAG Reset, 80h: JTAG Interface Disable')