import csv
from time import sleep
from EmergencyMsg import Translator

file_dir = '/home/CanDumpAnalysis/NodesFormatted/'


class AnalyseLines:
    def __init__(self, file_name):
        self._file_name = file_name
        self.com_objs = []
        self.com_objs_count = []

    def read_lines(self, start, stop):
        EmergencyTrans = Translator()

        my_file = file_dir + str(self._file_name)
        with open(my_file, 'r') as in_file:
            reader = csv.reader(in_file)
            for i, line in enumerate(reader):
                if start <= i <= stop:
                    if line[1] == '0x80':
                        try:
                            print i, EmergencyTrans(line), line[4]
                        except ValueError as msg:
                            print msg
                            sleep(0.5)
                    else:
                        self.count_com_cobjs(line, i)
                        pass
                elif i > stop:
                    break
        self.print_counts()

    def print_counts(self):
        for index in range(len(self.com_objs)):
            print self.com_objs[index], self.com_objs_count[index]

    def count_com_cobjs(self, line, i):
        if ' ' in str(line):
            print i, line
        com_obj = line[2]
        if not self.com_objs.count(com_obj):
            self.com_objs.append(com_obj)
            self.com_objs_count.append(1)
        else:
            index = self.com_objs.index(com_obj)
            self.com_objs_count[index] += 1


if __name__ == '__main__':
    obj = AnalyseLines('18.csv')
    obj.read_lines(1916447-100, 1916447+100)
