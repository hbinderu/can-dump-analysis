Digital IO:
Self-recovery: 35
External Reset - digital: 2
External Reset - can, digital: 1
External Reset - can, analog, digital: 3
Bootloader is now in control, Microcontroller MCUCSR register contents: Watchdog Reset: 1
Life Guarding, CAN-controller has been reinitialized: 4
External Reset - can: 1


Nodeguards:
External Reset - digital: 2
External Reset - can, digital: 3
Self-recovery: 238
Life Guarding, CAN-controller has been reinitialized: 14
External Reset - analog: 12
External Reset - can: 1
External Reset - can, analog, digital: 4


ADC:
Bootloader is now in control, Microcontroller MCUCSR register contents: Watchdog Reset: 4
External Reset - digital: 2
External Reset - can, digital: 1
Self-recovery: 14
External Reset - can, analog, digital: 3
External Reset - analog: 1
External Reset - can: 1


--------------------------------------------------

Board:
Bootloader is now in control, Microcontroller MCUCSR register contents: Watchdog Reset: 4
Self-recovery: 259
External Reset - digital: 2
External Reset - can, digital: 1
External Reset - can, analog, digital: 3
Life Guarding, CAN-controller has been reinitialized: 11
External Reset - analog: 13
External Reset - can: 1


--------------------------------------------------

Board Digital Parts Only:
Self-recovery: 259
External Reset - digital: 2
External Reset - can, digital: 1
External Reset - can, analog, digital: 3
Bootloader is now in control, Microcontroller MCUCSR register contents: Watchdog Reset: 1
Life Guarding, CAN-controller has been reinitialized: 18
External Reset - analog: 12
External Reset - can: 1
