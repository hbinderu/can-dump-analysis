import os
from stat import *
import csv
from tqdm import tqdm

candump_dir = '/home/CanDumps/'
save_dir = '/home/CanDumpAnalysis/SeparatedBusses/'


def separate_busses():
    dumps = os.listdir(candump_dir)
    for items in dumps:
        save_item_dir = save_dir + str(items[0:len(items)-4]) + '/'
        if not os.path.exists(save_item_dir):
            os.makedirs(save_item_dir)

        with open(candump_dir + items) as in_file:
            for line in tqdm(in_file, ascii=True, desc=str(items)):
                if 'can' in line:
                    can_bus = line[line.find('can'): line.find('can')+4]
                    with open(save_item_dir + can_bus + '.csv', 'a') as fd:
                        fd.write(line)


if __name__ == '__main__':
    separate_busses()
